# Planning Center Beta #

Accessing the portal:

 * [Member's view](https://impactlifechurch.churchcenteronline.com/home)
 * [Admin's view](https://accounts.planningcenteronline.com/session/login/new)

This is a repository storing some notes from our church's beta test. We will list the tasks to be completed here as well.

## Tasks to complete ##

The tasks listed below are tasks that you should try as an individual and as a group(ministry or lifegroup). 
You as a ministry may decide to rely solely on Planning Center for your operations during this time.

1. Update Your Profile
1. Add people to your group
1. Lifegroup Tasks
	1. Mark Attendance(Parallel with current system)
	1. Registering Events
1. Ministry Tasks
	1. Band to use song features
	1. Rostering band, media and video
	1. Video and Media under the same team
	1. Adding of song, Item notes, description

If you have any queries or feedback, please add your comments or feedback in the issue tracker in this repository.

## Resources ##

* [Slides Presented](https://docs.google.com/presentation/d/1WqLnk2mU-sWDVHbx_nGzR9eGwQEfAvdLlRDur2H3W4Y/edit?usp=sharing)
* [Planning Center Documentation](https://support.planningcenteronline.com/hc/en-us)